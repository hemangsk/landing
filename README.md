Hello! Welcome to the coala.io landing page of the future! This is a WIP, the [coala/website](https://github.com/coala/website) repo is the current one.


![](frontend/screenshots/header.png)

## Run it in Docker

Clone the repository, then:

```
$ docker build -t coala/landing .
$ docker run -e "GITHUB_API_KEY=<your key>" -p 8000:8000 coala/landing
```

For debugging, use the `-t -i` arguments to see the output of the container.

## Get Started


```
$ git clone git@gitlab.com:coala/landing.git
$ cd landing
$ git fetch origin
$ git checkout hemangsk/django
```


### Backend
#### STEP 1
```
$ cd backend
$ pip install -r requirements.txt
$ python manage.py makemigrations
$ python manage.py migrate
```

#### STEP 2

Add GitHub api key in backend/coala_web/settings.py file

```
GITHUB_API_KEY = ''
```

#### STEP 3
After this, we have to start cron jobs, cron settings are in
`backend/coala_web/settings.py` file, in the variable CRONJOBS:

```
CRONJOBS = [
  ('*/200 * * * *', 'bears.cron.fetch_bears'),
  ('*/200 * * * *', 'org.cron.fetch_repos'),
  ('*/200 * * * *', 'org.cron.fetch_contributors')
]
```

The time of jobs can be modified accordingly. [Reference for time codes](http://crontab.guru)

#### STEP 4
Then start the crons with

```
$ python manage.py crontab add
```

#### STEP 5
Now we're ready to start Django server
```
$ python manage.py runserver
```


### Frontend

#### STEP 1
```
$ cd frontend
$ bundle install
$ python -m SimpleHTTPServer 8090
```

#### STEP 2
Inside resources/js/config.js, Add appropriate api link
```
api_link = ''
```


Hooray! The site is up and running at http://localhost:8090

-----

### Basic Structure

HOME TAB

![](frontend/screenshots/img1.png)



LANGUAGES TAB

![](frontend/screenshots/img2.png)



BEAR DETAILS VIEW

![](frontend/screenshots/img3.png)



GET INVOLVED TAB

![](frontend/screenshots/img4.png)



MEET THE COMMUNITY SECTION

![](frontend/screenshots/img5.png)

-----

## Plans
[The Roadmap for coala.io](https://gitlab.com/coala/landing/issues/3)

-----
## Contribution Guidelines
[Refer Here](https://github.com/coala/coala/blob/master/README.rst)


-----
## License
![](https://img.shields.io/github/license/coala/coala.svg)
