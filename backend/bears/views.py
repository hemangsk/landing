import collections

from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from bears.models import Bear

from brake.decorators import ratelimit


@ratelimit(block=True, rate='50/m')
@csrf_exempt
@require_http_methods(["GET", "POST"])
def home(request):
    if request.method == "GET":
        res = {}
        bears = Bear.objects.all()
        for bear in bears:
            res[bear.name] = {
                "desc": bear.from_json()["metadata"]["desc"],
                "languages": bear.from_json()["LANGUAGES"]
            }
        res = collections.OrderedDict(sorted(res.items()))
        return JsonResponse(res, safe=False)


@ratelimit(block=True, rate='50/m')
@csrf_exempt
@require_http_methods(["GET", "POST"])
def search(request):
    if request.method == "GET":
        bear_query = request.GET['bear']
        bear = Bear.objects.filter(name=bear_query)[0]
        bear.click_count += 1
        bear.save()
        return JsonResponse(bear.from_json())


@ratelimit(block=True, rate='50/m')
@csrf_exempt
@require_http_methods(["GET", "POST"])
def analytics(request):
    if request.method == "GET":
        res = {}
        bears = Bear.objects.all()
        for bear in bears:
            res[bear.name] = bear.click_count
        res = sorted(res.items(), key=lambda x: (x[1],x[0]), reverse=True)
        return JsonResponse(res, safe=False)
