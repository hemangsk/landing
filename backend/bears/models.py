from django.db import models
import json


class Bear(models.Model):
    name = models.TextField(default=None, primary_key=True)
    data = models.TextField(default=None)
    click_count = models.IntegerField(default=0)

    def from_json(self):
        return json.loads(self.data)

    def to_json(self, value):
        self.data = json.dumps(value)
